apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: goldpinger-metrics
  labels:
    app: goldpinger
    release: kube-prometheus-stack
spec:
  namespaceSelector:
    matchNames:
      - "{{ $.Release.Namespace }}"
  selector:
    matchLabels:
      app.kubernetes.io/name: goldpinger
  endpoints:
    - port: http
      path: /metrics
